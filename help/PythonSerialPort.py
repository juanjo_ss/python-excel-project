import serial
import re
from time import sleep
import json
import requests
from openpyxl import Workbook
import datetime


"""
Script de Python que accede a los datos que llegan desde el periferico al
central, los encapsula en una trama HTTP y envia al servicio web
"""

# Crea un archivo excel con una hoja de trabajo
wb = Workbook()
ws = wb.active

# Adquirimos la fecha y la hora desde la libreria datetime
fecha = datetime.datetime.now().date()
hora = datetime.datetime.now().time()

# Abre el puerto serial COM12
DiscoveryPort = serial.Serial(port='COM12', baudrate=115200, bytesize=serial.EIGHTBITS,
                              parity=serial.PARITY_NONE, timeout=2)

# Expresion_Regular1 = "T: 02873 I 541 85D6 5E16 A116 5F16 1B13 P"
Expresion_Regular1 = r"^T: (?P<Timer_Data>[-|+]?[0-F]{1,8}) I (?P<IMU_a_x1>[-|+]?[0-F]{1,4}) "\
                    r"(?P<IMU_h_x1>[-|+]?[0-F]{1,4}) (?P<IMU_a_y1>[-|+]?[0-F]{1,4}) "\
                    r"(?P<IMU_h_y1>[-|+]?[0-F]{1,4}) (?P<IMU_a_z1>[-|+]?[0-F]{1,4}) "\
                    r"(?P<IMU_h_z1>[-|+]?[0-F]{1,4}) P$"

# Expresion_Regular1 = "T: 028732EF P"
Expresion_Regular2 = r"^T: (?P<Timer_Data_Central>[-|+]?[0-F]{1,8}) P$"

# Expresion_Regular1 = "T: 028732EF P"
Expresion_Regular3 = r"^C: (?P<Contador_Tramas>[-|+]?[0-F]{1,8}) P$"

# Compila la Expresion_Regular1 para analizarla con otra String
parser1 = re.compile(Expresion_Regular1)

# Compila la Expresion_Regular1 para analizarla con otra String
parser2 = re.compile(Expresion_Regular2)

# Compila la Expresion_Regular1 para analizarla con otra String
parser3 = re.compile(Expresion_Regular3)


def decode_trama(command, parser):
    """
    Decodifica y compara con una Expresion_Regular1 una trama de datos que entra
    por parametro

    :type command: String
    :param command: StringPuertoSerie que llegan desde el COM12

    :type parser: re.compile()
    :param parser: Compila la Expresion_Regular1 para analizarla con otra String
    """
    return parser.search(command)


def save_to_exel(data_imu):
    """
    Extrae los datos de la imu que llegan del puertoSerie cuando es igual
    a la Expresion_Regular1 y los guarda en un archivo Excel en formato de
    0-4 G.

    :type data_imu: StringHex
    :param data_imu: datos que llegan desde el COM12
    """
    TimerTemp = int(data_imu["Timer_Data"], 16)
    Eje_X_A_4G = 4*(int(data_imu["IMU_a_x1"], 16))/255
    Eje_X_H_4G = 4*(int(data_imu["IMU_h_x1"], 16))/255
    Eje_Y_A_4G = 4*(int(data_imu["IMU_a_y1"], 16))/255
    Eje_Y_H_4G = 4*(int(data_imu["IMU_h_y1"], 16))/255
    Eje_Z_A_4G = 4*(int(data_imu["IMU_a_z1"], 16))/255
    Eje_Z_H_4G = 4*(int(data_imu["IMU_h_z1"], 16))/255

    # Guarda los valores de cada data_imu de la trama de la IMU en una fila de excel
    ws.append([fecha, TimerTemp, Eje_X_A_4G, Eje_X_H_4G, Eje_Y_A_4G,
               Eje_Y_A_4G, Eje_Z_A_4G, Eje_Z_H_4G])

    # En cada itereacion del bucle while guardamos los datos en el archivo excel

def show_info_trama(data_imu):
    """
    Muestra los datos de la trama IMU

    :type data_imu: StringHex
    :param data_imu: datos que llegan desde el COM12
    """
    TimerTemp = int(data_imu["Timer_Data"], 16)
    Eje_X_A = int(data_imu["IMU_a_x1"], 16)
    Eje_X_H = int(data_imu["IMU_h_x1"], 16)
    Eje_Y_A = int(data_imu["IMU_a_y1"], 16)
    Eje_Y_H = int(data_imu["IMU_h_y1"], 16)
    Eje_Z_A = int(data_imu["IMU_a_z1"], 16)
    Eje_Z_H = int(data_imu["IMU_h_z1"], 16)

    print("\nTiempo: " + str(TimerTemp) + " Ticks de Reloj" +
          "\nEje_X_A: " + str(Eje_X_A) + "\nEje_X_H: " + str(Eje_X_H) +
          "\nEje_Y_A: " + str(Eje_Y_A) + "\nEje_Y_H: " + str(Eje_Y_H) +
          "\nEje_Z_A: " + str(Eje_Z_A) + "\nEje_Z_H: " + str(Eje_Z_H))

def show_info_trama2(data_Timer_Central):
    """
    Muestra los datos del Timer del Central

    :type data_Timer_Central: StringHex
    :param data_Timer_Central: datos que llegan desde el COM12
    """
    TimerCentral = int(data_Timer_Central["Timer_Data_Central"], 16)
    print(TimerCentral)
    TiempoSegundos = 0.006*(TimerCentral) #Un tick cada 6ms
    #Throughput250kBytes = 250000/TiempoSegundos
    #Throughput10kBytes = 10000/TiempoSegundos
    print("El numero de Ticks del Timer del Central es: "+ str(TimerCentral) + " en segundos: " + str(TiempoSegundos))
    #print("El valor del Throughput: "+ str(Throughput10kBytes) + " Bps")
    #ws.append([TimerCentral, TiempoSegundos, Throughput10kBytes])

def show_info_trama3(Contador_Tramas):
    """
    Muestra los datos del Timer del Central

    :type data_Timer_Central: StringHex
    :param data_Timer_Central: datos que llegan desde el COM12
    """
    Numero_Tramas_Rx= int(Contador_Tramas["Contador_Tramas"], 16)
    print("Llegaron : "+ str(Numero_Tramas_Rx) + " tramas")

"""
def post_to_Emoncms_web_service(data_imu):

    TimerTemp = int(data_imu["Timer_Data"], 16)
    Eje_X_A_4G = 2*(int(data_imu["IMU_a_x1"], 16))/255
    Eje_X_H_4G = 4*(int(data_imu["IMU_h_x1"], 16))
    Eje_Y_A_4G = 2*(int(data_imu["IMU_a_y1"], 16))/255
    Eje_Y_H_4G = 4*(int(data_imu["IMU_h_y1"], 16))
    Eje_Z_A_4G = 2*(int(data_imu["IMU_a_z1"], 16))/255
    Eje_Z_H_4G = 4*(int(data_imu["IMU_h_z1"], 16))
    Sensor1 = (int(data_imu["IMU_h_y1"], 16))
    Sensor2 = (int(data_imu["IMU_a_z1"], 16))
    Sensor3 = (int(data_imu["IMU_h_z1"], 16))

    dataTrama = '{vTimer:str(TimerTemp), vAccel_X:str(Eje_X_A_4G), vAccel_Y:str(Eje_Y_A_4G), vAccel_Z:str(Eje_Z_A_4G), vGyro_X:str(Eje_X_H_4G), vGyro_Y:str(Eje_Y_H_4G), vGyro_Z:str(Eje_Z_H_4G), vSensor1:str(Sensor1), vSensor2:str(Sensor2), vSensor3:str(Sensor3)}'

    data = [('node', '1'), ('data', dataTrama),('apikey', writeAPIKEY),]
    #Se realiza el post y se comprueba que este todo correcto con print()
    response = requests.post('https://emoncms.org/input/post', data=data)
    print(response.text)
    sleep(2)
"""

def post_to_web_service(data_imu):
    """
    Metodologia para envío de los datos al servicio web

    :type data_imu: StringHex
    :param data_imu: datos que llegan desde el COM12
    """
    #Direccion HTTP de la base de datos MongoDB donde se guardan los datos
    url= 'http://localhost:3000/api/sensores'

    TimerTemp = int(data_imu["Timer_Data"], 16)
    Eje_X_A_4G = 4*(int(data_imu["IMU_a_x1"], 16))/255
    Eje_X_H_4G = 4*(int(data_imu["IMU_h_x1"], 16))/255
    Eje_Y_A_4G = 4*(int(data_imu["IMU_a_y1"], 16))/255
    Eje_Y_H_4G = 4*(int(data_imu["IMU_h_y1"], 16))/255
    Eje_Z_A_4G = 4*(int(data_imu["IMU_a_z1"], 16))/255
    Eje_Z_H_4G = 4*(int(data_imu["IMU_h_z1"], 16))/255
    Sensor1 = (int(data_imu["IMU_h_y1"], 16))
    Sensor2 = (int(data_imu["IMU_a_z1"], 16))
    Sensor3 = (int(data_imu["IMU_h_z1"], 16))

    #Trama de ejemplo para probar que los datos llegan a MongoDB
    dataTrama = {'nombreExperimento': "Experimento1", 'vTimer': "Timer: " + str(TimerTemp),
            'vAccel_X': "Accel_X: " + str(Eje_X_A_4G), 'vAccel_Y': "Accel_Y: " + str(Eje_Y_A_4G),
            'vAccel_Z': "Accel_Z: " + str(Eje_Z_A_4G), 'vGyro_X': "Gyro_X: " + str(Eje_X_H_4G),
            'vGyro_Y': "Gyro_Y: " + str(Eje_Y_H_4G), 'vGyro_Z': "Gyro_Z: " + str(Eje_Z_H_4G), 'vSensor1': "Sensor1: " + str(Sensor1),
            'vSensor2': "Sensor2: " + str(Sensor2), 'vSensor3': "Sensor3: " + str(Sensor3)}

    dataIMU_json = json.dumps(dataTrama)
    #Se realiza el post y se comprueba que este todo correcto con print()
    r = requests.post(url=url, data=dataIMU_json, headers=headers)
    print(r.text)
    print(dataIMU_json)

def post_to_UBIDOTS_web_service(data_imu):
    """
    Metodologia para envío de los datos al servicio web para representarlos
    de manera gráfica

    :type data_imu: StringHex
    :param data_imu: datos que llegan desde el COM12
    """
    # Tipo de contenido que conlleva la trama del post
    headers = {'content-type': 'application/json'}

    # Temporary and revocable keys to be used in your API requests.
    token = 'ImtUyQ6iIE8K6PBJHyIsZWvyjbrEf6'

    TimerTemp = int(data_imu["Timer_Data"], 16)
    Eje_X_A_4G = 2*(int(data_imu["IMU_a_x1"], 16))/255
    Eje_X_H_4G = 2*(int(data_imu["IMU_h_x1"], 16))
    Eje_Y_A_4G = 2*(int(data_imu["IMU_a_y1"], 16))/255
    Eje_Y_H_4G = 2*(int(data_imu["IMU_h_y1"], 16))
    Eje_Z_A_4G = 2*(int(data_imu["IMU_a_z1"], 16))/255
    Eje_Z_H_4G = 2*(int(data_imu["IMU_h_z1"], 16))
    Sensor1 = (int(data_imu["IMU_h_y1"], 16))*10
    Sensor2 = (int(data_imu["IMU_a_z1"], 16))*9
    Sensor3 = (int(data_imu["IMU_h_z1"], 16))*11

    # Datos a enviar en formato requests desde el formato curl
    data = '{"vAccelX":'+str(Eje_X_A_4G)+', "vAccelY":'+str(Eje_Y_A_4G)+', "vAccelZ":'+str(Eje_Z_A_4G)+', "vGyroX":'+str(Eje_X_H_4G)+', "vGyroY":'+str(Eje_Y_H_4G)+', "vGyroZ":'+str(Eje_Z_H_4G)+', "vSensor1":'+str(Sensor1)+', "vSensor2":'+str(Sensor2)+', "vSensor3":'+str(Sensor3)+'}'

    # Envio de los datos medianto post
    response = requests.post('http://things.ubidots.com/api/v1.6/devices/sensores/?token=A1E-ImtUyQ6iIE8K6PBJHyIsZWvyjbrEf6', headers=headers, data=data)

    print(response.text)


def main():

    """
    Funcion principal del script su obejtivo es:
    Lee el numero de bytes que entran por el COM12
    Muestra por pantalla el numero de bits de cada linea
    Guarda los datos que vienen por la linea del COM12 en una vairable de string
    Compara los datos del puertoSerie con la expresion regular
    Si lo que llega es igual a la Expresion_Regular1
    Si no, muestra por pantalla el numero de bits de cada linea
    """
    while True:

        # Lee el numero de bytes que entran por el COM12
        n_bytes = DiscoveryPort.in_waiting
        if n_bytes > 0:
            # Muestra por pantalla el numero de bits de cada linea
            #print(DiscoveryPort.in_waiting)

            # Guarda los datos que vienen por la linea del COM12 en una vairable de string
            datosPuertoSerie = DiscoveryPort.readline().decode('utf-8', "replace").strip()

            # Compara los datos del puertoSerie con la expresion regular
            dataSensoresPeripheral = decode_trama(datosPuertoSerie, parser1)
            datoTimerCentral = decode_trama(datosPuertoSerie, parser2)
            contadorTramas = decode_trama(datosPuertoSerie, parser3)
            #datosTest = decode_trama(Test1, parser)
            # Si lo que llega es igual a la Expresion_Regular1
            if dataSensoresPeripheral:
                show_info_trama(dataSensoresPeripheral)
                post_to_UBIDOTS_web_service(dataSensoresPeripheral)

            if (datoTimerCentral):
                show_info_trama2(datoTimerCentral)

            if(contadorTramas):
                show_info_trama3(contadorTramas)

            else:
                # Muestra por pantalla el numero de bits de cada linea
                print(str(datosPuertoSerie))
        else:
            sleep(0.2)

if __name__ == '__main__':
    main()
