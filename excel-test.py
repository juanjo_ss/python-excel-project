from openpyxl import load_workbook
import datetime
import re

wb = load_workbook(filename='files-test/export2020412.xlsx')
sheet_ranges = wb['Movimientos']
regExConcept1 = r'Compra * ?(?P<conceptType>[^\]]*) En* (?P<conceptShop>[^\]]*), * (?P<conceptPlace>[^\]]*), Tarj. :(?P<conceptCard>[^\]]{7})'
# Compiles the regEx to analyse
parser1 = re.compile(regExConcept1)


class DataExtracted:
    date = ''
    conceptType = ''
    conceptShop = ''
    conceptPlace = ''
    conceptCard = ''
    amount = ''


def parse_string(string, parser):
    # searches for first occurrence of RE pattern within string
    return parser.search(string)


def inject_info(string_analysed):
    DataExtracted.conceptType = string_analysed["conceptType"]
    DataExtracted.conceptShop = string_analysed["conceptShop"]
    DataExtracted.conceptPlace = string_analysed["conceptPlace"]
    DataExtracted.conceptCard = string_analysed["conceptCard"]
    return DataExtracted

def read_excel():
    data_extracted = DataExtracted()
    for i in range(8, 81):
        data_extracted.date = sheet_ranges['A' + str(i)].value
        concept = sheet_ranges['C' + str(i)].value
        data_extracted.amount = sheet_ranges['D' + str(i)].value
        string_analysed = parse_string(concept, parser1)
        if string_analysed:
            data_extracted = inject_info(string_analysed)
            print(data_extracted.date, data_extracted.conceptType, str(data_extracted.amount)+'€')
        else:
            print("No match!!")


read_excel()
